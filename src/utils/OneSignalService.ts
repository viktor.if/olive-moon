import OneSignal from 'react-native-onesignal';
import {Platform} from 'react-native';

export class OneSignalService {
  /**
   * Start OneSignal
   */
  static start = (token: string) => {
    OneSignal.setLogLevel(6, 0);
    OneSignal.setAppId(token);

    /**
     * Prompt for push on iOS
     */
    if (Platform.OS === 'ios') {
      OneSignal.promptForPushNotificationsWithUserResponse(response => {
        console.log('Prompt response:', response);
      });
    }
  };

  /**
   * Method for handling notifications opened
   * Android & iOS
   */
  static setNotificationOpenedHandler = (cb: (data: any) => void) => {
    OneSignal.setNotificationOpenedHandler(notification => {
      console.log('OneSignal: notification opened:', notification);

      cb(notification.notification);
    });
  };

  /**
   * Method for handling notifications received while app in foreground
   * Android & iOS
   */
  static setNotificationWillShowInForegroundHandler = (
    cb: (data: any) => void,
  ) => {
    OneSignal.setNotificationWillShowInForegroundHandler(
      notificationReceivedEvent => {
        console.log(
          'OneSignal: notification will show in foreground:',
          notificationReceivedEvent,
        );

        let notification = notificationReceivedEvent.getNotification();
        console.log('notification: ', notification);

        cb(notification);

        const data = notification.additionalData;
        console.log('additionalData: ', data);

        // Complete with null means don't show a notification.
        notificationReceivedEvent.complete(notification);
      },
    );
  };
}
