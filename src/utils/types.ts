export type TNotificationMessage = {
  id: string;
  title?: string;
  body: string;
  time: string;
};
