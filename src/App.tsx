import React, {useEffect, useState} from 'react';
import {OSNotification} from 'react-native-onesignal';
import moment from 'moment';

import {
  SafeAreaView,
  StatusBar,
  Platform,
  FlatList,
  View,
  Text,
} from 'react-native';

import styles from './styles';
import {TNotificationMessage} from './utils/types';
import {AsyncStorageService} from './utils/AsyncStorageService';
import {OneSignalService} from './utils/OneSignalService';

const STORAGE_KEY = '@messages';
const ONESIGNAL_TOKEN = 'e1a156c8-ad8a-4621-94aa-1d80b6d9cea2';

export default () => {
  const [messages, setMessages] = useState<TNotificationMessage[]>([]);

  // Receive a new message
  const onReceiveMessage = (msg: OSNotification) => {
    setMessages(prevState => {
      const existsAlready = !!prevState.find(
        item => item.id === msg.notificationId,
      );

      if (existsAlready) {
        return prevState;
      }

      const nextState = [
        {
          id: msg.notificationId,
          title: msg.title?.trim(),
          body: msg.body.trim(),
          time: moment().format('HH:mm DD MMM'),
        },
        ...prevState,
      ];
      AsyncStorageService.save(STORAGE_KEY, nextState);
      return nextState;
    });
  };

  // Run OneSignal service
  useEffect(() => {
    OneSignalService.start(ONESIGNAL_TOKEN);
    OneSignalService.setNotificationOpenedHandler(onReceiveMessage);
    OneSignalService.setNotificationWillShowInForegroundHandler(
      onReceiveMessage,
    );
  }, []);

  // Get data from the storage to the state
  useEffect(() => {
    AsyncStorageService.get(STORAGE_KEY, setMessages);
  }, []);

  // Status bar
  useEffect(() => {
    StatusBar.setBarStyle('dark-content');

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#fff');
      StatusBar.setTranslucent(false);
    }
  }, []);

  return (
    <SafeAreaView style={styles.screen}>
      <FlatList
        data={messages}
        keyExtractor={item => item.id}
        renderItem={({item}) => <ListItem {...item} />}
        contentContainerStyle={styles.list}
        ListEmptyComponent={
          <Text style={styles.emptyListText}>No messages</Text>
        }
      />
    </SafeAreaView>
  );
};

/** List item */
const ListItem = (message: TNotificationMessage): JSX.Element => (
  <View style={styles.listItem}>
    {!!message.title && (
      <Text style={styles.titleText} numberOfLines={1}>
        {message.title}
      </Text>
    )}
    <Text style={styles.bodyText} numberOfLines={5}>
      {message.body}
    </Text>
    <Text style={styles.timeText} numberOfLines={5}>
      {message.time}
    </Text>
  </View>
);
